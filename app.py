from inspect import getinnerframes
import numpy as np
import base64
from io import BytesIO
from flask import Flask, request, jsonify
from flask_cors import CORS
from keras.models import load_model

from utils import getImgFromAngles

model = load_model('model1.h5')

app = Flask(__name__)
CORS(app)

@app.route("/")
def hello_world():
    return "<p>Hello, World 2!</p>"

@app.route('/predictSpell', methods=['POST'])
def predictSpell():
    req = request.get_json()

    angles = req['attitudes']

    cropped = getImgFromAngles(angles)

    npImg = np.array(cropped)
    # rawPixels = list(npImg.astype("int64").flatten())
    npImg = npImg.astype('float32')
    npImg = np.reshape(npImg, (1,28,28,1))
    pred = model.predict(npImg)

    predictedDigit = str(pred.argmax())
    # print('digit', pred.argmax())
    # cropped.show()

    # print(rawPixels)
    # flat = list([str(i) for i in rawPixels])
    # print(type(flat), flat.shape)

    im_file = BytesIO()
    cropped.save(im_file, format="PNG")
    im_bytes = im_file.getvalue()  # im_bytes: image in binary format.
    im_b64 = base64.b64encode(im_bytes).decode('utf-8')
    
    return jsonify({ 'prediction': predictedDigit, 'img': im_b64    })
