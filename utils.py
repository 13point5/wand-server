import math
from PIL import Image, ImageDraw

WIDTH = 1000
HEIGHT = 1000

def getDist(old, new):
    diff = (new - old) * (180.0 / math.pi)

    if diff < 0:
        diff += 360
    
    if diff > 180:
        diff -= 360
    
    return round(-700 * math.tan(diff * (math.pi / 180.0)))

def getPointsFromAngles(angles):
    points = []

    if len(angles) == 0:
        return points

    old = angles[0]
    for i in range(1, len(angles)):
        new = angles[i]
        x = getDist(old[0], new[0]) + WIDTH/2
        y = getDist(old[1], new[1]) + HEIGHT/2
        points.append((x,y))
    
    return points

def getCornersOfImg(img):
    bottom = [WIDTH/2, HEIGHT]
    for y in range(HEIGHT):
        for x in range(WIDTH):
            pixel = img.getpixel((x, y))
            if pixel > 0:
                bottom = [x,y]
                break

    top = [WIDTH/2, 0]
    for y in range(HEIGHT-1, -1, -1):
        for x in range(WIDTH):
            pixel = img.getpixel((x, y))
            if pixel > 0:
                top = [x,y]
                break

    right = [0, HEIGHT/2]
    for x in range(WIDTH):
        for y in range(HEIGHT):
            pixel = img.getpixel((x, y))
            if pixel > 0:
                right = [x,y]
                break
            
    left = [WIDTH, HEIGHT/2]
    for x in range(WIDTH-1, -1, -1):
        for y in range(HEIGHT):
            pixel = img.getpixel((x, y))
            if pixel > 0:
                left = [x,y]
                break

    topLeft = (max(0, left[0] - 50), max(0, top[1] - 50))
    bottomRight = (min(WIDTH, right[0] + 50), min(HEIGHT, bottom[1] + 50))
            
    return topLeft, bottomRight

def getImgFromAngles(angles):

    points = getPointsFromAngles(angles)

    im = Image.new('1', (WIDTH, HEIGHT))
    draw = ImageDraw.Draw(im)

    for i in range(1, len(points)):
        prev = points[i-1]
        curr = points[i]
        draw.line((prev[0], prev[1], curr[0], curr[1]), fill='white', width=30)
    
    im = im.transpose(Image.FLIP_TOP_BOTTOM)
    draw = ImageDraw.Draw(im)

    topLeft, bottomRight = getCornersOfImg(im)

    cropped = im.crop((topLeft[0], topLeft[1], bottomRight[0], bottomRight[1]))
    # cropped.show()
    cropped = cropped.resize((28,28))

    return cropped
